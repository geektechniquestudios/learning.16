package com.geektechnique.chucknorrisjokes.services;

public interface JokesService {

    String getJoke();
}
